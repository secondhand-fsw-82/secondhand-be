'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    return queryInterface.addColumn("products", "status", {
      type: Sequelize.STRING,
    })
  },

  async down (queryInterface, Sequelize) {
    return queryInterface.removeColumn("products", "status")
  }
};
