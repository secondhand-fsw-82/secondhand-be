'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    return queryInterface.addColumn("products", "description", {
      type: Sequelize.STRING,
      defaultValue: "",
    })
  },

  async down (queryInterface, Sequelize) {
    return queryInterface.removeColumn("products", "description")
  }
};
