const db = require('../models')
const CATEGORIES_MODEL = db.category

async function list(req, res) {
    try {
        const categories = await CATEGORIES_MODEL.findAll()

        res.json({
            status: true,
            message: "Kategori ditemukan",
            data: categories
        })
    } catch (error) {
        res.status(500)
        res.json({
            status: false,
            message: error.message
        })
    }
}

module.exports = {
    list
}