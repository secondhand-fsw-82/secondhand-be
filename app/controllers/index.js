const main = require("./main");
const auth = require('./auth.controller')
const user = require("./user.controller")
const product = require("./product.controller")
const order = require("./order.controller")
const categories = require("./category.controller")
const notification = require("./notification.controller")

module.exports = {
  main,
  auth,
  user,
  categories,
  product,
  order,
  notification
};

