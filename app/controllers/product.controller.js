const db = require("../models");
const fs = require("fs");
const { readdir } = require("fs/promises")
const sequelize = require("sequelize");
const { Op } = require("sequelize");
const auth = require("./auth.controller.js");
const PRODUCTS_MODEL = db.products;
const CATEGORIES_MODEL = db.category;
const CUSOTMERS_MODEL = db.customers;
const ORDER_MODEL = db.order;
const GALLERY_MODEL = db.galleries;
const cloudinary = require("../../config/cloudinary");

async function create(req, res) {
    const { name, price, category_id, description, status } = req.body;
    try {
        const tokenPayload = await auth.getCurrentUser(req, res)
        const idUser = tokenPayload.id;

        const check = await PRODUCTS_MODEL.findAll({
            where: {
                merchant_id: idUser
            }
        })

        if (check.length >= 4) {
            res.json({
                status: false,
                message: "Maaf tidak boleh mengepost produk lebih dari 4 produk, silahkan hapus produk yang belum terjual atau tunggu salah satu produkmu terjual",
            });

            return;
        }

        const product = await PRODUCTS_MODEL.create({
            name: name,
            merchant_id: idUser,
            price: price,
            category_id: category_id,
            description: description,
            status: status
        })

        let images = [];
        if (req.files) {
            for (const file of req.files) {
                const urls = await new Promise((resolve, reject) => {
                    const fileBase64 = file.buffer.toString('base64');
                    const filess = `data:${file.mimetype};base64,${fileBase64}`;
                    cloudinary.uploader.upload(filess, { folder: `Products-Customers` }, async function (err, result) {
                        const images = await GALLERY_MODEL.create({
                            product_id: product.id,
                            image: result.url,
                        });
                        resolve(images)

                    });
                })
                images.push(urls);
            }
        }

        res.statusCode = 200;
        res.json({
            status: true,
            message: "Berhasil membuat produk",
            data: {
                product,
                images
            },
        });
    } catch (error) {
        console.log(error);
        res.statusCode = 500;
        res.json({
            status: false,
            message: error.message,
        });
    }
}

async function get(req, res) {
    try {
        const { category, } = req.query;
        const where = {
            status: "available",
        }

        if (category) {
            where.category_id = category
        }

        const response = await PRODUCTS_MODEL.findAll({
            where,
            include: [
                {
                    model: CUSOTMERS_MODEL,
                    attributes: ["id", "full_name", "email", "image", "phone", "address", "city"],
                },
                {
                    model: CATEGORIES_MODEL,
                    attributes: ["id", "name", "description"],
                },
                {
                    model: GALLERY_MODEL,
                    attributes: ["id", "product_id", "image"],
                }
            ],
        });

        if (response.length === 0) {
            res.json({
                status: false,
                message: "Data produk kosong",
            });

            return;
        }

        const products = await Promise.all(response.map(async (arr) => {

            return {
                id: arr.id,
                name: arr.name,
                merchant: arr.customer,
                price: arr.price,
                category: arr.category,
                description: arr.description,
                image: arr.galleries,
                status: arr.status,
                createdAt: arr.createdAt,
                updatedAt: arr.updatedAt,
            }
        }));
        res.json({
            status: true,
            message: "Berhasil mendapatkan produk",
            data: products,
        });
    } catch (error) {
        console.log(error);
        res.statusCode = 500;
        res.json({
            status: false,
            message: error.message,
        });
    }
}

async function find(req, res) {
    const { id } = req.params;
    try {
        const response = await PRODUCTS_MODEL.findByPk(id, {
            include: [
                {
                    model: CUSOTMERS_MODEL,
                    attributes: ["id", "full_name", "email", "image", "phone", "address", "city"],
                },
                {
                    model: CATEGORIES_MODEL,
                    attributes: ["id", "name", "description"],
                },
                {
                    model: GALLERY_MODEL,
                    attributes: ["id", "product_id", "image"],
                }
            ],
        });

        if (!response) {
            res.statusCode = 404;
            res.json({
                status: false,
                message: "Data produk tidak ada",
            });

            return;
        }

        const product = {
            id: response.id,
            name: response.name,
            merchant: response.customer,
            price: response.price,
            category: response.category,
            description: response.description,
            image: response.galleries,
            status: response.status,
            createdAt: response.createdAt,
            updatedAt: response.updatedAt,
        };

        res.json({
            status: true,
            message: "Produk berhasil ditemukan",
            data: product,
        });
    } catch (error) {
        console.log(error);
        res.json({
            status: false,
            message: error.message,
        });
    }
}

async function updateProduct(req, res) {
    const { name, price, category_id, description, status } = req.body;
    const { id } = req.params;
    try {
        const response = await PRODUCTS_MODEL.findByPk(id)

        if (!response) {
            res.statusCode = 404;
            res.json({
                status: false,
                message: "Produk tidak ditemukan",
            });

            return;
        }

        await PRODUCTS_MODEL.update({
            name: name,
            price: price,
            category_id: category_id,
            description: description,
            status: status
        }, {
            where: {
                id: id
            }
        })

        const gallery = await GALLERY_MODEL.findAll({
            where: {
                product_id: id
            }
        })

        let images = [];

        if (req.files.length !== 0) {
            if (gallery.length !== 0) {
                for (var i = 0; i < gallery.length; i++) {
                    const idTemp = gallery[i].id
                    await GALLERY_MODEL.destroy({
                        where: {
                            id: idTemp
                        }
                    })
                    cloudinary.uploader.destroy(gallery[i].image.substring(61, 100))
                }
            }

            for (const file of req.files) {
                const urls = await new Promise((resolve, reject) => {
                    const fileBase64 = file.buffer.toString('base64');
                    const filess = `data:${file.mimetype};base64,${fileBase64}`;
                    cloudinary.uploader.upload(filess, { folder: `Products-Customers` }, async function (err, result) {
                        const images = await GALLERY_MODEL.create({
                            product_id: response.id,
                            image: result.url,
                        });
                        resolve(images)

                    });
                })
                images.push(urls);
            }
        } else {
            images = gallery
        }

        const product = await PRODUCTS_MODEL.findByPk(id)

        res.statusCode = 200;
        res.json({
            status: true,
            message: "Berhasil mengupdate produk",
            data: {
                product,
                images
            },
        });

    } catch (error) {
        console.log(error);
        res.statusCode = 500;
        res.json({
            status: false,
            message: error.message,
        });
    }
}

async function deleteProduct(req, res) {
    const { id } = req.params;
    try {
        const response = await PRODUCTS_MODEL.findByPk(id, {
            include: [
                {
                    model: CUSOTMERS_MODEL,
                    attributes: ["id", "full_name", "email", "image", "phone", "address", "city"],
                },
                {
                    model: CATEGORIES_MODEL,
                    attributes: ["id", "name", "description"],
                },
            ],
        });

        if (!response) {
            res.statusCode = 404;
            res.json({
                status: false,
                message: "Data produk tidak ada",
            });

            return;
        }

        const imageProduct = await GALLERY_MODEL.findAll({
            where: {
                product_id: id
            }
        })
        let cloudImage;

        if (imageProduct.length > 0) {
            for (var i = 0; i < imageProduct.length; i++) {
                cloudImage = imageProduct[i].image.substring(61, 100);
                console.log(cloudImage)
                cloudinary.uploader.destroy(cloudImage);
            }
        }

        await GALLERY_MODEL.destroy({ where: { product_id: id } });
        await PRODUCTS_MODEL.destroy({ where: { id: id } });

        res.statusCode = 200;
        res.json({
            status: true,
            message: "Data berhasil dihapus",
        });
    } catch (error) {
        console.log(error);
        res.statusCode = 500;
        res.json({
            status: false,
            message: error.message,
        });
    }
}

async function getAllSellerProducts(req, res) {
    try {
        const tokenPayload = await auth.getCurrentUser(req, res)
        const idUser = tokenPayload.id;

        const response = await PRODUCTS_MODEL.findAll({
            where: {
                merchant_id: idUser
            },
            include: [
                {
                    model: CUSOTMERS_MODEL,
                    attributes: ["id", "full_name", "email", "image", "phone", "address", "city"],
                },
                {
                    model: CATEGORIES_MODEL,
                    attributes: ["id", "name", "description"],
                },
                {
                    model: GALLERY_MODEL,
                    attributes: ["id", "product_id", "image"],
                }
            ],
        });

        if (response.length === 0) {
            res.json({
                status: false,
                message: "Data produk kosong",
            });

            return;
        }

        const products = await Promise.all(response.map(async (arr) => {

            return {
                id: arr.id,
                name: arr.name,
                merchant: arr.customer,
                price: arr.price,
                category: arr.category,
                description: arr.description,
                image: arr.galleries,
                createdAt: arr.createdAt,
                updatedAt: arr.updatedAt,
            }
        }));
        res.json({
            status: true,
            message: "Berhasil mendapatkan produk",
            data: products,
        });

    } catch (error) {
        console.log(error);
        res.statusCode = 500;
        res.json({
            status: false,
            message: error.message,
        });
    }
}

async function getInterestedProducts(req, res) {
    try {
        const tokenPayload = await auth.getCurrentUser(req, res)
        const idUser = tokenPayload.id;

        const response = await ORDER_MODEL.findAll({
            where: {
                [Op.or]: [{ status: "pending" }, { status: "waiting" }]
            },
            include: [
                {
                    model: CUSOTMERS_MODEL,
                    attributes: ["id", "full_name", "email", "image", "phone", "address", "city"],
                },
                {
                    model: PRODUCTS_MODEL,
                    attributes: ["id", "name", "merchant_id", "price", "category_id", "description"],
                    include: [
                        {
                            model: CUSOTMERS_MODEL,
                            attributes: ["id", "full_name", "email", "image", "phone", "address", "city"],
                            required: false
                        },
                        {
                            model: CATEGORIES_MODEL,
                            attributes: ["id", "name", "description"],
                            required: false
                        },
                        {
                            model: GALLERY_MODEL,
                            attributes: ["id", "product_id", "image"],
                        }
                    ]
                },
            ],
        })

        if (response.length === 0) {
            res.json({
                status: false,
                message: "Tidak ada produk yang diminati",
                data: []
            });

            return;
        }

        const products = await Promise.all(response.map(async (arr) => {
            if (arr.product.merchant_id === idUser) {
                return {
                    id: arr.id,
                    fix_value: arr.fix_value,
                    buyer: arr.customer,
                    product: {
                        id: arr.product.id,
                        name: arr.product.name,
                        seller: arr.product.customer,
                        price: arr.product.price,
                        category: arr.product.category,
                        description: arr.product.description,
                        image: arr.product.galleries,
                    },
                    createdAt: arr.createdAt,
                    updatedAt: arr.updatedAt,
                }
            }
        }));

        let result = []
        for (var i = 0; i < products.length; i++) {
            if (products[i] !== undefined) {
                result.push(products[i])
            }
        }

        if (result.length === 0) {
            res.json({
                status: true,
                message: "Produk kamu belum ada yang diminati",
                data: [],
            });

            return;
        }

        res.json({
            status: true,
            message: "Berhasil mendapatkan produk yang diminati",
            data: result,
        });

    } catch (error) {
        console.log(error);
        res.statusCode = 500;
        res.json({
            status: false,
            message: error.message,
        });
    }
}

async function getSoldProducts(req, res) {
    try {
        const tokenPayload = await auth.getCurrentUser(req, res)
        const idUser = tokenPayload.id;

        const response = await ORDER_MODEL.findAll({
            where: {
                status: "sold"
            },
            include: [
                {
                    model: CUSOTMERS_MODEL,
                    attributes: ["id", "full_name", "email", "image", "phone", "address", "city"],
                },
                {
                    model: PRODUCTS_MODEL,
                    attributes: ["id", "name", "merchant_id", "price", "category_id", "description"],
                    include: [
                        {
                            model: CUSOTMERS_MODEL,
                            attributes: ["id", "full_name", "email", "phone", "address", "city"],
                            required: false
                        },
                        {
                            model: CATEGORIES_MODEL,
                            attributes: ["id", "name", "description"],
                            required: false
                        },
                        {
                            model: GALLERY_MODEL,
                            attributes: ["id", "product_id", "image"],
                        }
                    ]
                },
            ],
        })

        if (response.length === 0) {
            res.json({
                status: false,
                message: "Tidak ada produk yang terjual",
            });

            return;
        }

        const products = await Promise.all(response.map(async (arr) => {
            if (arr.product.merchant_id === idUser) {
                return {
                    id: arr.id,
                    fix_value: arr.fix_value,
                    buyer: arr.customer,
                    product: {
                        id: arr.product.id,
                        name: arr.product.name,
                        seller: arr.product.customer,
                        price: arr.product.price,
                        category: arr.product.category,
                        description: arr.product.description,
                        image: arr.product.galleries,
                    },
                    createdAt: arr.createdAt,
                    updatedAt: arr.updatedAt,
                }
            }
        }));

        let result = []
        for (var i = 0; i < products.length; i++) {
            if (products[i] !== undefined) {
                result.push(products[i])
            }
        }

        if (result.length === 0) {
            res.json({
                status: false,
                message: "Produk kamu belum ada yang terjual",
            });

            return;
        }

        res.json({
            status: true,
            message: "Berhasil mendapatkan produk yang terjual",
            data: result,
        });

    } catch (error) {
        console.log(error);
        res.statusCode = 500;
        res.json({
            status: false,
            message: error.message,
        });
    }
}

module.exports = {
    create,
    list: get,
    find,
    updateProduct,
    deleteProduct,
    getAllSellerProducts,
    getInterestedProducts,
    getSoldProducts,
};
