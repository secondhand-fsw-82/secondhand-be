const db = require('../models')
const ORDER_MODEL = db.order
const PRODUCTS_MODEL = db.products
const BUYERS_MODEL = db.customers
const CATEGORIES_MODEL = db.category;
const NOTIFICATION_MODEL = db.notification;
const GALLERY_MODEL = db.galleries;
const auth = require("./auth.controller.js");
const product = require("./product.controller.js");
const { Op, or } = require("sequelize");

async function list(req, res) {
    try {
        const order = await ORDER_MODEL.findAll()

        res.json({
            status: true,
            message: "Berhasil mendapatkan order list",
            data: order
        })
    } catch (error) {
        console.log(error)
        res.statusCode = 500
        res.json({
            status: false,
            title: error.name,
            message: error.message
        })
    }
}

async function create(req, res) {
    const { buyer_id, product_id, fix_value, quantity, status = "pending" } = req.body
    try {
        if (!buyer_id || !product_id) {
            res.statusCode = 400
            res.json({
                status: false,
                message: "Buyer ID dan Product ID tidak boleh kosong"
            })

            return
        }

        const buyer = await BUYERS_MODEL.findByPk(buyer_id)

        if (!buyer) {
            res.statusCode = 400
            res.json({
                status: false,
                message: "Buyer ID tidak ditemukan!"
            })

            return
        }

        const product = await PRODUCTS_MODEL.findByPk(product_id)

        if (!product) {
            res.statusCode = 400
            res.json({
                status: false,
                message: "Product ID tidak ditemukan!"
            })
        }

        if (product.merchant_id === buyer_id) {
            res.statusCode = 400
            res.json({
                status: false,
                message: "Tidak boleh membeli produk yang dijual sendiri!"
            })
        }

        if (fix_value < 1000) {
            res.statusCode = 400
            res.json({
                status: false,
                message: "Harga yang ditawar tidak boleh kurang dari 1.000 Rupiah"
            })

            return
        }

        if (quantity <= 0) {
            res.statusCode = 400
            res.json({
                status: false,
                message: "Jumlah yang dibeli tidak boleh kurang dari 0"
            })

            return
        }

        const order = await ORDER_MODEL.create({
            buyer_id,
            product_id,
            fix_value,
            quantity,
            quantity,
            status
        })

        const notif = await NOTIFICATION_MODEL.create({
            order_id: order.id,
            seller_id: product.merchant_id,
            buyer_id: buyer_id,
            title: "Penawaran Produk",
            isRead: false
        })

        res.statusCode = 201
        res.json({
            status: true,
            message: "Orderan berhasil dibuat",
            data: order,
            notification: notif
        })

    } catch (error) {
        console.log(error)
        res.statusCode = 500
        res.json({
            status: false,
            title: error.name,
            message: error.message
        })
    }
}

async function update(req, res) {
    const { status } = req.body
    const { id } = req.params
    try {
        if (!id || !status) {
            res.statusCode = 400
            res.json({
                status: false,
                message: "ID Order dan status tidak boleh kosong!"
            })

            return
        }

        const query = {
            where: {
                id
            }
        }

        const findOne = await ORDER_MODEL.findOne(query)
        await findOne.update({ status })

        // if (!order[0]) {
        //     res.statusCode = 404
        //     res.json({
        //         status: false,
        //         title: "Gagal update status order",
        //         message: "ID Order tidak ditemukan",
        //     })

        //     return
        // }

        let notifikasi;

        const response = await ORDER_MODEL.findByPk(id)
        const searchNotif = await NOTIFICATION_MODEL.findOne({
            where: {
                order_id: id
            }
        })

        if (status === "rejected") {
            const notif = await NOTIFICATION_MODEL.create({
                order_id: searchNotif.order_id,
                seller_id: searchNotif.seller_id,
                buyer_id: searchNotif.buyer_id,
                title: "Penawaran Produk Ditolak",
                message: "Tawaran kamu ditolak penjual",
                isRead: false
            })
            notifikasi = await NOTIFICATION_MODEL.findOne({ where: { id: notif.id } })
        } else if (status === "sold") {
            await PRODUCTS_MODEL.update({
                status: "sold"
            }, {
                where: {
                    id: response.product_id
                }
            })

            const findAll = await ORDER_MODEL.findAll({
                where: {
                    id: {
                        [Op.not]: id
                    },
                    product_id: findOne.product_id,
                }
            })


            findAll.map(result => {
                result.set({ status: "rejected" })
                result.save()
            })

            const notif = await NOTIFICATION_MODEL.create({
                order_id: searchNotif.order_id,
                seller_id: searchNotif.seller_id,
                buyer_id: searchNotif.buyer_id,
                title: "Penawaran Produk Berhasil Disepakati",
                message: "Terima Kasih telah membeli produk ini",
                isRead: false
            })
            notifikasi = await NOTIFICATION_MODEL.findOne({ where: { id: notif.id } })
        } else if (status === "waiting") {
            const notif = await NOTIFICATION_MODEL.create({
                order_id: searchNotif.order_id,
                seller_id: searchNotif.seller_id,
                buyer_id: searchNotif.buyer_id,
                title: "Penawaran Produk Diterima",
                message: "Kamu akan segera dihubungi penjual via whatsapp",
                isRead: false
            })
            notifikasi = await NOTIFICATION_MODEL.findOne({ where: { id: notif.id } })
        }

        res.json({
            status: true,
            message: "Berhasil update status order",
            notification: notifikasi
        })


    } catch (error) {
        console.log(error)
        res.statusCode = 500
        res.json({
            status: false,
            title: error.name,
            message: error.message
        })
    }
}

async function getOrdersByIdOrderOrIdProduct(req, res) {
    try {
        const { id, product_id } = req.query;
        const where = {}

        if (id) {
            where.id = id
        }

        if (product_id) {
            where.product_id = product_id
        }

        const response = await ORDER_MODEL.findAll({
            where,
            include: [
                {
                    model: BUYERS_MODEL,
                    attributes: ["id", "full_name", "email", "image", "phone", "address", "city"],
                },
                {
                    model: PRODUCTS_MODEL,
                    attributes: ["id", "name", "merchant_id", "price", "category_id", "description"],
                    include: [
                        {
                            model: BUYERS_MODEL,
                            attributes: ["id", "full_name", "email", "image", "phone", "address", "city"],
                            required: false
                        },
                        {
                            model: CATEGORIES_MODEL,
                            attributes: ["id", "name", "description"],
                            required: false
                        },
                        {
                            model: GALLERY_MODEL,
                            attributes: ["id", "product_id", "image"],
                        }
                    ]
                },
            ],
        })

        if (response.length === 0) {
            res.json({
                status: false,
                message: "Tidak ada transaksi yang pernah dilakukan",
            });

            return;
        }

        const order = await Promise.all(response.map(async (arr) => {
            return {
                id: arr.id,
                fix_value: arr.fix_value,
                quantity: arr.quantity,
                status: arr.status,
                buyer: arr.customer,
                product: {
                    id: arr.product.id,
                    name: arr.product.name,
                    seller: arr.product.customer,
                    price: arr.product.price,
                    category: arr.product.category,
                    description: arr.product.description,
                    image: arr.product.galleries,
                },
                createdAt: arr.createdAt,
                updatedAt: arr.updatedAt,
            }
        }));

        let result = []
        for (var i = 0; i < order.length; i++) {
            if (order[i] !== undefined) {
                result.push(order[i])
            }
        }


        if (result.length === 0) {
            res.statusCode = 400
            res.json({
                status: false,
                message: "Order tidak ditemukan"
            })

            return
        }

        res.json({
            status: true,
            message: "Order berhasil didapatkan",
            data: order,
        });

    } catch (error) {
        console.log(error)
        res.statusCode = 500
        res.json({
            status: false,
            title: error.name,
            message: error.message
        })
    }
}

async function historyTransactionBuyer(req, res) {
    try {
        const tokenPayload = await auth.getCurrentUser(req, res)
        const idUser = tokenPayload.id;

        const response = await ORDER_MODEL.findAll({
            include: [
                {
                    model: BUYERS_MODEL,
                    attributes: ["id", "full_name", "email", "image", "phone", "address", "city"],
                },
                {
                    model: PRODUCTS_MODEL,
                    attributes: ["id", "name", "merchant_id", "price", "category_id", "description", "image"],
                    include: [
                        {
                            model: BUYERS_MODEL,
                            attributes: ["id", "full_name", "email", "image", "phone", "address", "city"],
                            required: false
                        },
                        {
                            model: CATEGORIES_MODEL,
                            attributes: ["id", "name", "description"],
                            required: false
                        },
                        {
                            model: GALLERY_MODEL,
                            attributes: ["id", "product_id", "image"],
                        }
                    ]
                },
            ],
        })

        if (response.length === 0) {
            res.json({
                status: false,
                message: "Tidak ada transaksi yang dilakukan",
            });

            return;
        }

        const products = await Promise.all(response.map(async (arr) => {
            if (arr.buyer_id === idUser) {
                return {
                    buyer: arr.customer,
                    status_order: arr.status,
                    product: {
                        id: arr.product.id,
                        name: arr.product.name,
                        seller: arr.product.customer,
                        price: arr.product.price,
                        category: arr.product.category,
                        description: arr.product.description,
                        image: arr.product.galleries,
                    },
                    createdAt: arr.createdAt,
                    updatedAt: arr.updatedAt,
                }
            }
        }));

        let result = []
        for (var i = 0; i < products.length; i++) {
            if (products[i] !== undefined) {
                result.push(products[i])
            }
        }

        if (result.length === 0) {
            res.json({
                status: true,
                message: "Kamu belum pernah melakukan transaksi",
            });

            return;
        }

        res.json({
            status: true,
            message: "Berhasil mendapatkan history transaksi",
            data: result,
        });

    } catch (error) {
        console.log(error)
        res.statusCode = 500
        res.json({
            status: false,
            title: error.name,
            message: error.message
        })
    }
}

module.exports = {
    create,
    update,
    list,
    getOrdersByIdOrderOrIdProduct,
    historyTransactionBuyer
}