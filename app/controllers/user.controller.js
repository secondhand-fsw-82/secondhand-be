require("dotenv").config();
const auth = require("./auth.controller.js");
const db = require("../models");
const CUSTOMERS_MODEL = db.customers;
const { kirimEmail } = require("../../helpers");
const cloudinary = require("../../config/cloudinary");

function validateEmail(email) {
  return email.match(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
}

async function register(req, res) {
  try {
    const fname = req.body.full_name;
    const email = req.body.email.toLowerCase();
    const password = req.body.password;

    if (!fname || !email || !password) {
      res.statusCode = 400;
      res.json({
        status: false,
        message: "Nama lengkap, email, dan password tidak boleh kosong!",
      });

      return;
    }

    if (!validateEmail(email)) {
      res.statusCode = 400;
      res.json({
        status: false,
        message: "Email yang dimasukkan tidak valid",
      });

      return;
    }

    if (password.length < 8) {
      res.statusCode = 400;
      res.json({
        status: false,
        message: "Password minimal 8 karakter!",
      });

      return;
    }

    const encryptPassword = await auth.encryptPassword(password);

    const existUser = await CUSTOMERS_MODEL.findOne({
      where: {
        email: email,
      },
    });

    if (existUser) {
      return res.status(401).json({
        status: false,
        message: "Email sudah digunakan!",
      });
    }

    const user = await CUSTOMERS_MODEL.create({
      full_name: fname,
      email: email,
      password: encryptPassword,
      createdAt: new Date(),
      updatedAt: new Date(),
    });

    const token = auth.createToken({
      id: user.id,
      email: user.email,
    });

    await user.update({
      emailToken: token,
      isVerified: false,
    });

    const templateEmail = {
      from: '"SECONDHAND" <secondhandfsw8.2@gmail.com>',
      to: email,
      subject: "Verifikasi email anda",
      html: `<h2> Halo ${user.full_name}! Terima kasih telah registrasi di website kami </h2>
             <h4> Silahkan verifikasi terlebih dahulu email anda untuk melanjutkan... </h4>
             <a href="${process.env.CLIENT_URL}/verifyemail?token=${token}">Verifikasi Email Anda</a>`,
    };
    kirimEmail(templateEmail);

    res.statusCode = 200;
    res.json({
      status: true,
      message: "Berhasil membuat user",
      data: user,
    });
  } catch (error) {
    console.log(error);
    res.statusCode = 500;
    res.json({
      status: false,
      message: error.message,
    });
  }
}

async function login(req, res) {
  try {
    const email = req.body.email.toLowerCase();
    const password = req.body.password;

    const user = await CUSTOMERS_MODEL.findOne({
      where: {
        email: email,
      },
    });

    if (!user) {
      return res.status(401).json({
        message: "Email tidak ditemukan!",
      });
    }

    const matchPass = await auth.checkPassword(user.password, password);

    if (!matchPass) {
      return res.status(401).json({
        message: "Password anda salah!",
      });
    }

    const token = auth.createToken({
      id: user.id,
      email: user.email,
    });

    return res.status(200).json({
      message: "User berhasil login",
      data: user,
      token: token,
    });
  } catch (error) {
    res.statusCode = 500;
    res.json({
      status: false,
      message: error.message,
    });
  }
}

async function profile(req, res) {
  try {
    const profile = await CUSTOMERS_MODEL.findByPk(req.params.id);

    if (!profile) {
      return res.status(401).json({
        message: "Profile user tidak ditemukan!",
      });
    }

    return res.status(200).json({
      message: "Profile user berhasil ditemukan!",
      data: profile,
    });
  } catch (error) {
    res.statusCode = 500;
    res.json({
      status: false,
      message: error.message,
    });
  }
}

async function updatePofile(req, res) {
  const { full_name, city, address, phone } = req.body;
  try {
    const tokenPayload = await auth.getCurrentUser(req, res)
    const idUser = tokenPayload.id;

    const user = await CUSTOMERS_MODEL.findOne({
      where: {
        id: idUser,
      },
    });

    if (!user) {
      return res.status(400).json({
        status: false,
        message: "User tidak ditemukan",
      });
    }

    if (!req.file) {
      await user.update({
        full_name: full_name,
        city: city,
        address: address,
        phone: phone,
        updatedAt: new Date()
      })
        .then(() => {
          return res.status(200).json({
            status: true,
            message: "Data profile berhasil diupdate",
            data: user
          });
        })
        .catch((error) => {
          return res.status(400).json({
            status: false,
            message: "Data profile gagal diupdate",
            data: error
          });
        })
    } else {
      if (user.image !== null) {
        let cloudImage = user.image.substring(61, 99);
        cloudinary.uploader.destroy(cloudImage);
      }

      const fileBase64 = req.file.buffer.toString("base64")
      const file = `data:${req.file.mimetype};base64,${fileBase64}`

      const imageUpload = await cloudinary.uploader.upload(file, { folder: `Profile-Customers` }, function (err, result) {
        if (!!err) {
          console.log(err);
          return res.status(400).json({
            message: "Gagal upload file!",
          });
        }

        return result
      });

      await user.update({
        full_name: full_name,
        city: city,
        address: address,
        phone: phone,
        image: imageUpload.url,
        updatedAt: new Date()
      })
        .then(() => {
          return res.status(200).json({
            status: true,
            message: "Data profile berhasil diupdate",
            data: user
          });
        })
        .catch((error) => {
          return res.status(400).json({
            status: false,
            message: "Data profile gagal diupdate",
            data: error
          });
        })
    }

  } catch (error) {
    res.statusCode = 500;
    res.json({
      status: false,
      message: error.message,
    });
  }
}

async function forgetPassword(req, res) {
  try {
    const { email } = req.body;

    const user = await CUSTOMERS_MODEL.findOne({
      where: {
        email: email,
      },
    });

    if (!user) {
      return res.status(400).json({
        status: false,
        message: "Email tidak ditemukan",
      });
    }

    const token = await auth.createToken({ id: user.id });
    await user.update({
      resetPasswordToken: token,
    });

    const templateEmail = {
      from: '"SECONDHAND" <secondhandfsw8.2@gmail.com>',
      to: email,
      subject: "Link reset password",
      html: `<h2> Halo ${user.full_name}!</h2>
             <h4> Silahkan klik link di bawah untuk melakukan reset password </h4>
             <a href="${process.env.CLIENT_URL}/resetpassword/${token}">Reset Password Anda</a>`,
    };
    kirimEmail(templateEmail);

    return res.status(200).json({
      status: true,
      message: "Link reset password berhasil terkirim, silahkan cek email anda",
    });
  } catch (error) {
    res.statusCode = 500;
    res.json({
      status: false,
      message: error.message,
    });
  }
}

async function resetPassword(req, res) {
  try {
    const { token, password } = req.body;

    const user = await CUSTOMERS_MODEL.findOne({
      where: {
        resetPasswordToken: token,
      },
    });

    if (user) {
      const hashPassword = await auth.encryptPassword(password);
      await user.update({
        password: hashPassword,
      });

      return res.status(200).json({
        status: true,
        message: "Password berhasil diganti",
      });
    }
  } catch (error) {
    res.statusCode = 500;
    res.json({
      status: false,
      message: error.message,
    });
  }
}

async function verifyEmail(req, res) {
  try {
    const { token } = req.body;

    const user = await CUSTOMERS_MODEL.findOne({
      where: {
        emailToken: token,
      },
    });

    if (user) {
      await user.update({
        emailToken: null,
        isVerified: true,
      });
    } else {
      res.status(400).json({
        status: false,
        message: "Email anda belum terverifikasi",
      });
    }

    return res.status(200).json({
      status: true,
      message: "Email berhasil terverifikasi, silahkan kembali ke website",
    });
  } catch (error) {
    res.statusCode = 500;
    res.json({
      status: false,
      message: error.message,
    });
  }
}

async function verifyEmailUser(req, res, next) {
  try {
    const { email } = req.body;

    const user = await CUSTOMERS_MODEL.findOne({
      where: {
        email: email,
      },
    });

    if (!user) {
      res.status(400).json({
        status: false,
        message: "Akun tidak ditemukan",
      });
    } else {
      if (user.isVerified) {
        next();
      } else {
        res.status(400).json({
          status: false,
          message: "Silahkan cek email anda untuk melakukan verifikasi terlebih dahulu",
        });
      }
    }
  } catch (error) {
    res.statusCode = 500;
    res.json({
      status: false,
      message: error.message,
    });
  }
}

async function updateStatus(req, res) {
  try {
    const { email, isVerified = true } = req.body;

    const query = {
      where: {
        email,
      },
    };

    await CUSTOMERS_MODEL.update({ isVerified }, query);

    res.json({
      status: true,
      message: "Status verfikasi user telah diupdate",
    });
  } catch (error) {
    res.statusCode = 500;
    res.json({
      status: false,
      message: error.message,
    });
  }
}

async function whoami(req, res) {
  try {
    const tokenPayload = await auth.getCurrentUser(req, res);
    const idUser = tokenPayload.id;

    const data = await CUSTOMERS_MODEL.findOne({
      where: {
        id: idUser,
      },
    });

    if (!user) {
      return res.status(400).json({
        status: false,
        message: "User tidak ditemukan",
      });
    }

    res.status(200).json({
      status: true,
      data,
    });
    return data;
  } catch (error) {
    res.statusCode = 500;
    res.json({
      status: false,
      message: error.message,
    });
  }
}

module.exports = {
  register,
  login,
  profile,
  updatePofile,
  forgetPassword,
  resetPassword,
  verifyEmail,
  verifyEmailUser,
  updateStatus,
  whoami,
};
