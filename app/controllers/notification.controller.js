const db = require('../models');
const NOTIFICATION_MODEL = db.notification;
const PRODUCTS_MODEL = db.products;
const ORDER_MODEL = db.order;
const GALLERY_MODEL = db.galleries;
const { Op } = require("sequelize");
const auth = require("./auth.controller.js");

async function getAllNotificationUser(req, res) {
    try {
        const tokenPayload = await auth.getCurrentUser(req, res)
        const idUser = tokenPayload.id;

        const response = await NOTIFICATION_MODEL.findAll({
            where: {
                [Op.or]: [{seller_id: idUser}, {buyer_id: idUser}]
            },
            include: [
                {
                    model: ORDER_MODEL,
                    attributes: ["id", "buyer_id", "product_id", "fix_value", "quantity", "status"],
                    include: [
                        {
                            model: PRODUCTS_MODEL,
                            attributes: ["id", "name", "merchant_id", "price", "category_id", "description"],
                            include: [
                                {
                                    model: GALLERY_MODEL,
                                    attributes: ["id", "product_id", "image"],
                                }
                            ]
                        }
                    ]
                }
            ]
        })

        if(response.length === 0) {
            res.statusCode = 400
            res.json({
                status: false,
                message: "Belum ada notifikasi yang masuk"
            })

            return
        }

        const notif = await Promise.all(response.map(async (arr) => {
            return {
                id: arr.id,
                order_id: arr.order_id,
                seller_id: arr.seller_id,
                buyer_id: arr.buyer_id,
                title: arr.title,
                message: arr.message,
                isRead: arr.isRead,
                product_name: arr.order.product.name,
                price: arr.order.product.price,
                fix_value: arr.order.fix_value,
                image: arr.order.product.galleries,
                createdAt: arr.createdAt,
                updatedAt: arr.updatedAt,
            }
        }));

        res.statusCode = 201
        res.json({
            status: true,
            message: "Berhasil mendapatkan notifikasi",
            data: notif
        })
    } catch (error) {
        console.log(error)
        res.statusCode = 500
        res.json({
            status: false,
            title: error.name,
            message: error.message
        })
    }
}

async function updateNotification(req, res) {
    const { id } = req.params;
    try {
        const check = await NOTIFICATION_MODEL.findByPk(id)

        if(!check) {
            res.statusCode = 404
            res.json({
                status: false,
                message: "Notifikasi tidak ditemukan",
            })

            return
        }

        await NOTIFICATION_MODEL.update({
            isRead: true
        }, {
            where: {
                id: id
            }
        })

        const notif = await NOTIFICATION_MODEL.findByPk(id)

        res.statusCode = 201
        res.json({
            status: true,
            message: "Berhasil mendapatkan notifikasi",
            notifikasi: notif
        })

    } catch (error) {
        console.log(error)
        res.statusCode = 500
        res.json({
            status: false,
            title: error.name,
            message: error.message
        })
    }
}

async function getNotificationById(req, res) {
    const { id } = req.params;
    try {
        const notif = await NOTIFICATION_MODEL.findByPk(id)

        if(!notif) {
            res.statusCode = 404
            res.json({
                status: false,
                message: "Notifikasi tidak ditemukan",
            })

            return
        }

        res.statusCode = 201
        res.json({
            status: true,
            message: "Berhasil mendapatkan notifikasi",
            notifikasi: notif
        })

    } catch (error) {
        console.log(error)
        res.statusCode = 500
        res.json({
            status: false,
            title: error.name,
            message: error.message
        })
    }
}

module.exports = {
    getAllNotificationUser,
    updateNotification,
    getNotificationById
}