"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class order extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.customers, {
        foreignKey: "buyer_id",
        targetKey: "id",
      });
      this.belongsTo(models.products, {
        foreignKey: "product_id",
        targetKey: "id",
      });
      this.hasMany(models.notification, {
        foreignKey: "order_id"
      })
    }
  }
  order.init(
    {
      buyer_id: DataTypes.INTEGER,
      product_id: DataTypes.INTEGER,
      fix_value: DataTypes.FLOAT,
      quantity: DataTypes.INTEGER,
      status: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "order",
    }
  );
  return order;
};
