'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class products extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.category, {
        foreignKey: 'category_id',
        targetKey: 'id'
      })
      this.belongsTo(models.customers, {
        foreignKey: 'merchant_id',
        targetKey: 'id'
      })
      this.belongsToMany(models.order, {
        through: "OrderDetails"
      })
      this.hasMany(models.galleries, {
        foreignKey: 'product_id'
      })
    }
  }
  products.init({
    name: DataTypes.STRING,
    merchant_id: {
      type: DataTypes.INTEGER,
      references: {
        model: 'customers',
        key: 'id'
      }
    },
    price: DataTypes.INTEGER,
    category_id: {
      type: DataTypes.INTEGER,
      references: {
        model: 'category',
        key: 'id'
      }
    },
    description:{
      type: DataTypes.STRING,
      defaultValue: "",
    }, 
    status: DataTypes.STRING,
  }, {
    sequelize,
    modelName: 'products',
  });
  return products;
};