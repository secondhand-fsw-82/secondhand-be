const express = require("express");
const router = express.Router();
const fs = require("fs");
const { user, categories, product, auth, order, notification } = require("../controllers/");
const multer = require("multer");
const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        const { merchant_id, name } = req.body;
        const dir = `uploads/products-${merchant_id}/${name}`;

        if (!fs.existsSync(dir)) {
            fs.mkdirSync(dir, { recursive: true });
        }

        cb(null, dir);
    },
    filename: (req, file, cb) => {
        const filename = file.originalname;
        const fileSplit = filename.split(".");
        const fileExt = fileSplit[fileSplit.length - 1];
        const uniqueSuffix = Date.now() + "-" + (Math.round(Math.random() * 1e9).toString() + "." + fileExt);
        cb(null, file.fieldname + "-" + uniqueSuffix);
    },
});
const upload = multer({ storage });
const uploadOnMemory = require("../middlewares/uploadOnMemory");

// All User
router.post("/registration", user.register);
router.post("/login", user.verifyEmailUser, user.login);
router.get("/profile/:id", auth.authorize, user.profile)
router.put("/updateprofile", auth.authorize, uploadOnMemory.single("image"), user.updatePofile)
router.put("/forgetpassword", user.forgetPassword);
router.put("/resetpassword", user.resetPassword);
router.put("/verifyemail", user.verifyEmail);
router.put("/status", user.updateStatus);
router.get("/whoami", auth.authorize, user.whoami);

// Categories
router.get("/categories", categories.list)

// Products
router.post("/product", auth.authorize, uploadOnMemory.array("products", 4), product.create);
router.get("/product", product.list);
router.get("/sellerproduct", auth.authorize, product.getAllSellerProducts)
router.get("/product/:id", product.find);
router.put("/product/:id", auth.authorize, uploadOnMemory.array("products", 4), product.updateProduct);
router.delete("/product/:id", auth.authorize, product.deleteProduct);
router.get("/interestedproduct", auth.authorize, product.getInterestedProducts);
router.get("/soldproduct", auth.authorize, product.getSoldProducts);

// Orders
router.post("/orders", auth.authorize, order.create)
router.get("/orders", auth.authorize, order.list)
router.put("/order/:id", auth.authorize, order.update)
router.get("/ordersid", auth.authorize, order.getOrdersByIdOrderOrIdProduct)
router.get("/historyorder", auth.authorize, order.historyTransactionBuyer)

// Notifications
router.get("/notification", auth.authorize, notification.getAllNotificationUser)
router.get("/notification/:id", auth.authorize, notification.getNotificationById)
router.put("/notification/:id", auth.authorize, notification.updateNotification)

module.exports = router;
