require("dotenv").config();
var express = require("express");
var path = require("path");
var logger = require("morgan");
var bodyParser = require('body-parser')
var cors = require("cors")

var indexRouter = require("./app/routes");
var apiRouter = require("./app/routes/api");

var app = express();

app.use(logger("dev"));
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(cors())

app.use('/uploads', express.static('uploads'))

app.use("/", indexRouter.router);

// api router
app.use("/api", apiRouter);

app.use(indexRouter.onLost)
app.use(indexRouter.onError)

module.exports = app;
